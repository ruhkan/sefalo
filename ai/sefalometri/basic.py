import numpy as np
import math
import cv2
from PIL import Image, ImageDraw, ImageFont, ImageTk
from tkinter import filedialog, Tk, Button, Label, messagebox, Toplevel, simpledialog

points_skala = ['Start Point', 'End Point']
points_text_COMPLETED = points_skala + ['Nasion', 'Sella', 'A Point', 'B Point', 'Porion', 'Orbitale', 'Gonion', 'Menton', 'Pogonion', 'Gnathion', 'Posterior Nasal Spine', 'Anterior Nasal Spine', 'Articulare', 'U1 Incisal Edge', 'U1 Apex', 'L1 Incisal Edge', 'L1 Apex', 'Mesial Cusp Mandibular M1']
points_text = ['Start', 'End'] + ['N', 'S', 'A Point', 'B Point', 'Po', 'Or', 'Go', 'Me', 'Pog', 'Gn', 'PNS', 'ANS', 'Ar', 'U1I', 'U1A', 'L1I', 'L1A', 'M1']
points = []
current_point_idx = 0
results = {}
angles_result = {}
lengths_result = {}
between_result = {}

colors = [(255, 0, 0)] * len(points_text)
scale_ratio = 1

def zoom(event):
    global image, points  
    zoom_scale = 2
    zoom_box_size = 50 

    img_height, img_width = image.shape[:2]

    x, y = event.x, event.y

    x1, y1 = max(x - zoom_box_size, 0), max(y - zoom_box_size, 0)
    x2, y2 = min(x + zoom_box_size, img_width), min(y + zoom_box_size, img_height)

    zoom_area = image[y1:y2, x1:x2]

    zoom_area_resized = cv2.resize(zoom_area, (zoom_box_size * zoom_scale * 2, zoom_box_size * zoom_scale * 2), interpolation=cv2.INTER_LINEAR)

    center_x = zoom_box_size * zoom_scale
    center_y = zoom_box_size * zoom_scale
    cv2.circle(zoom_area_resized, (int(center_x), int(center_y)), 3,(222, 18, 18), -1)
    
    for point in points:
        if x1 <= point[0] <= x2 and y1 <= point[1] <= y2:
            zoom_point_x = (point[0] - x1) * zoom_scale
            zoom_point_y = (point[1] - y1) * zoom_scale
            cv2.circle(zoom_area_resized, (int(zoom_point_x), int(zoom_point_y)), 3, (0, 255, 0), -1)

    cv2.imshow('Zoom', zoom_area_resized)
    cv2.setWindowProperty('Zoom', cv2.WND_PROP_TOPMOST, 1)

def open_image():
    global image, img_label, points, current_point_idx, zoomed_image
    image_path = filedialog.askopenfilename(filetypes=[("Image files", "*.jpg;*.jpeg;*.png;*.bmp;")])
    if not image_path:
        return
    image = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
    zoomed_image = image.copy()
    if image is not None:
        # zoomed_image = image.copy()
        points = []
        current_point_idx = 0
        update_image()
        zoomed_image = image.copy()
        prompt_next_point()
    else:
        messagebox.showerror("Error", "Failed to load the image.")

def prompt_next_point():
    global current_point_idx, points_text
    if current_point_idx < len(points_text):
        next_point_text = points_text[current_point_idx]
        if current_point_idx < 2:
            next_point_text = points_skala[current_point_idx]
        messagebox.showinfo("Next Point", "Please mark the point: {}".format(next_point_text))

def calculate_lengthh(a, b):
    return np.linalg.norm(np.array(b) - np.array(a))

def calculate_length(a, b):
    return math.sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2) * scale_ratio

def calculate_angles(a, b, c):
    ba = np.array(a) - np.array(b)
    bc = np.array(c) - np.array(b)

    if np.linalg.norm(ba) == 0 or np.linalg.norm(bc) == 0:
        return 0

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
 
    cosine_angle = np.clip(cosine_angle, -1, 1)

    angle = np.arccos(cosine_angle)

    return np.degrees(angle)

def intersection_point(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y

def calculate_intersection_angle(point1, point2, point3, point4):
    intersection = intersection_point((point1, point2), (point3, point4))
    if intersection is None:
        return None

    angle1 = math.atan2(point2[1] - point1[1], point2[0] - point1[0])
    angle2 = math.atan2(point4[1] - point3[1], point4[0] - point3[0])

    return math.degrees((angle2 - angle1) % (2 * math.pi))

def click(event):
    global points, img_label, image, current_point_idx, points_text, scale_ratio
    x, y = event.x, event.y
    if current_point_idx < len(points_text):
        points.append((x, y))
        current_point_idx += 1
        update_image()

        if current_point_idx == 2: 
            paper_length = simpledialog.askfloat("Input", "Masukkan panjang kertas asli sefalogram dalam mm:")
            pixel_length = calculate_length(points[0], points[1])
            scale_ratio = paper_length / pixel_length
            # new_image_size = (int(image.shape[1] * scale_ratio), int(image.shape[0] * scale_ratio))
            # resize_image(new_image_size)
            # print(scale_ratio)
            # print(pixel_length)

        calculate_lengths()
        if len(points)>=5:
            calculate_angel()
        if len(points)>=10:
            calculate_between()

        if current_point_idx < len(points_text):
            prompt_next_point()
        else:
            print("Finish")

def resize_image(size):
    global image, zoomed_image, points
    image = cv2.resize(image, size)
    zoomed_image = image.copy()
    points = [(int(p[0] * scale_ratio), int(p[1] * scale_ratio)) for p in points]
    update_image()

def update_image():
    global image, points, image_with_points
    if image is None:
        return
    image_clone = image.copy()
    for i in range(len(points)):
        cv2.circle(image_clone, points[i], 3, colors[i], -1)
        cv2.putText(image_clone, points_text[i], (points[i][0], points[i][1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, colors[i], 1)
        while True :
            if i == 0 or i == 1:  
                cv2.line(image_clone, points[0], points[i], (0, 255, 0), 1)
            elif i == 2 or i == 3 :
                cv2.line(image_clone, points[2], points[i], (0, 255, 0), 1)
            elif i == 2 or i == 4 or i == 5:
                cv2.line(image_clone, points[2], points[i], (0, 255, 0), 1)
                cv2.line(image_clone, points[4], points[i], (0, 255, 0), 1)
            elif i == 6 or i == 7 : 
                cv2.line(image_clone, points[6], points[i], (0, 255, 0), 1)
            elif i == 8 or i == 9: 
                cv2.line(image_clone, points[8], points[i], (0, 255, 0), 1)
            elif i == 4 or i == 10 or i == 2: 
                cv2.line(image_clone, points[4], points[i], (0, 255, 0), 1)
                cv2.line(image_clone, points[2], points[i], (0, 255, 0), 1)
            elif i == 3 or i == 11 :#
                cv2.line(image_clone, points[3], points[i], (0, 255, 0), 1)
            # elif i == 8 or i == 0 :
            #     cv2.line(image_clone, points[0], points[i], (0, 255, 0), 1)
            elif i == 12 or i == 13 :
                cv2.line(image_clone, points[12], points[i], (0, 255, 0), 1)
            elif i == 3 or i == 14 or i == 8:#
                cv2.line(image_clone, points[3], points[i], (0, 255, 0), 1)
                cv2.line(image_clone, points[8], points[i], (0, 255, 0), 1)
            # elif i == 12 or i == 6 :
            #     cv2.line(image_clone, points[6], points[i], (0, 255, 0), 1)
            elif i == 15 or i == 16 :
                cv2.line(image_clone, points[15], points[i], (0, 255, 0), 1)
            elif i == 17  or i == 18 : 
                cv2.line(image_clone, points[17], points[i], (0, 255, 0), 1)
            elif i == 17 or i == 19 :
                cv2.line(image_clone, points[17], points[i], (0, 255, 0), 1)
            break
    img = Image.fromarray(image_clone)
    # pil_img = Image.fromarray(image_with_points)
    img.save("images/cephalometry_image_with_points.jpg")
    imgtk = ImageTk.PhotoImage(image=img)
    img_label.imgtk = imgtk
    img_label.configure(image=imgtk)


def calculate_lengths():
    global points, angles_result, lengths_result,points_text_COMPLETED
    # print("\n========== Cephalometry Results ==========")

    for i in range(len(points)):
        if i == 0 or i == 1 :
            length = calculate_length(points[0], points[i])
            length_AE = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[0], points_text_COMPLETED[i], length)
        elif i == 2 or i == 3 :
            length = calculate_length(points[2], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[2], points_text_COMPLETED[i], length)
        elif i == 2 or i == 4 :
            length = calculate_length(points[2], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[2], points_text_COMPLETED[i], length)
        elif i == 2 or i == 5 or i == 4:
            length = calculate_length(points[2], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[2], points_text_COMPLETED[i], length)
            length = calculate_length(points[4], points[i])
            length_text2 = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[i], points_text_COMPLETED[4], length)
        # elif i == 7 :
        #     length = calculate_length(points[6], points[i])
        #     length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[4], points_text_COMPLETED[i], length)
        elif i == 6 or i == 7 :
            length = calculate_length(points[6], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[6], points_text_COMPLETED[i], length)
        elif i == 8 or i == 9 :
            length = calculate_length(points[8], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[8], points_text_COMPLETED[i], length)
        elif i == 4 or i == 10 or i == 2:#
            length = calculate_length(points[4], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[4], points_text_COMPLETED[i], length)
            length = calculate_length(points[2], points[i])
            length_text2 = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[i], points_text_COMPLETED[2], length)
        elif i == 3 or i == 11 :
            length = calculate_length(points[3], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[3], points_text_COMPLETED[i], length)
        elif i == 12 or i == 13 :
            length = calculate_length(points[12], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[12], points_text_COMPLETED[i], length)
        elif i == 3 or i == 14 or i == 8:#
            length = calculate_length(points[3], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[3], points_text_COMPLETED[i], length)
            length = calculate_length(points[8], points[i])
            length_text2 = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[i], points_text_COMPLETED[8], length)
        elif i ==15 or i == 16 :
            length = calculate_length(points[15], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[15], points_text_COMPLETED[i], length)
        elif i ==17 or i == 18 :
            length = calculate_length(points[17], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[17], points_text_COMPLETED[i], length)
        elif i == 17 or i == 19 :
            length = calculate_length(points[17], points[i])
            length_text = "Length from {} to {}: {:.2f}".format(points_text_COMPLETED[17], points_text_COMPLETED[i], length)

    # if len(points)== 4:
    #     print(length_text2)
    if len(points)==3 or len(points)==7 or len(points)==9 or len(points)==13 or len(points)==16 or len(points)==18:
        next
    elif len (points)==2:
            print(length_AE,'mm')
    elif len (points) >= 4:
        print(length_text,'mm')
        if len (points)==6:
            print(length_text2,'mm')
        if len(points)==11:
            print(length_text2,'mm')
        if len(points)==15:
            print(length_text2,'mm')
        # lengths_result[point_text_COMPLETED[i]] = length_text
        # print(length_text)
        # lengths_result[points_text[i]] = length_text
    
    save_results()
    # steiner_analysis()
def calculate_angel():
    global points, angles_result, lengths_result,points_text_COMPLETED

    for i in range(len(points)) :
        if i == 3 or i == 2 or i == 4 :
            SNAangle = calculate_angles(points[points_text.index('S')], points[points_text.index('N')], points[points_text.index('A Point')])
            angleSNA = "Angle {} {} {}: {:.2f}°".format(points_text_COMPLETED[3], points_text_COMPLETED[2],points_text_COMPLETED[i], SNAangle)
        # elif i == 1 or i == 0 or i == 3 :
        #     SNBAngle = calculate_angles(points[points_text.index('S')], points[points_text.index('N')], points[points_text.index('B Point')])
        #     angleSNB = "Angel {} {} {}: {:.2f}".format(points_text_COMPLETED[1], points_text_COMPLETED[0],points_text_COMPLETED[i], SNBAngle)
        elif len(points)==6 and i == 3 or i == 2 or i == 5 or i == 4 or i == 2 or i == 5 :
            # SNaangle = calculate_angles(points[points_text.index('S')], points[points_text.index('N')], points[points_text.index('A Point')])
            SNbangle = calculate_angles(points[points_text.index('S')], points[points_text.index('N')], points[points_text.index('B Point')])
            ANBAngle = calculate_angles(points[points_text.index('A Point')], points[points_text.index('N')], points[points_text.index('B Point')])
            angleANB = "Angle ANB : {:.2f}°".format(ANBAngle)
            angleSNB = "Angle SNB : {:.2f}°".format(SNbangle)
        elif i == 2 or i == 4 or i == 10 :
            NAPogAngle = calculate_angles(points[points_text.index('N')], points[points_text.index('A Point')], points[points_text.index('Pog')])
            angelNAPog = "Angle {} {} {}: {:.2f}°".format(points_text_COMPLETED[2], points_text_COMPLETED[4],points_text_COMPLETED[i], NAPogAngle)
        elif i == 2 or i == 3 or i == 14:
            NSArAngle = calculate_angles(points[points_text.index('N')], points[points_text.index('S')], points[points_text.index('Ar')])
            angelNSAr = "Angle {} {} {}: {:.2f}°".format(points_text_COMPLETED[2], points_text_COMPLETED[3],points_text_COMPLETED[i], NSArAngle)

            SArGoAngle = calculate_angles(points[points_text.index('S')], points[points_text.index('Ar')], points[points_text.index('Go')])
            angelSArGo = "Angle {} {} {}: {:.2f}°".format(points_text_COMPLETED[3], points_text_COMPLETED[i],points_text_COMPLETED[8], SArGoAngle)

            ArGoMeAngle = calculate_angles(points[points_text.index('Ar')], points[points_text.index('Go')], points[points_text.index('Me')])
            angelArGoMe = "Angle {} {} {}: {:.2f}°".format(points_text_COMPLETED[14], points_text_COMPLETED[8],points_text_COMPLETED[9], ArGoMeAngle)

            bjorks_sum = NSArAngle + SArGoAngle + ArGoMeAngle
            sum_bjorks = "Björk's sum : {:.2f}".format(bjorks_sum)
    if len(points)==5 :
        print(angleSNA)
    elif len(points)==6:
        print(angleSNB)
        print(angleANB)
    elif len(points)==11:
        print(angelNAPog)
    elif len(points)==15:
        print(angelNSAr)
        print(angelSArGo)
        print(angelArGoMe)
        print(sum_bjorks)
    else :
        next
    # save_results()

def format_angle_value(angle):
    if angle is None or np.isnan(angle) or np.isinf(angle):
        return "N/A"
    return "{:.2f}".format(angle)

def calculate_between():
    global points, between_result, lengths_result, points_text_COMPLETED

    for i in range(len(points)):
        if i == 6 or i == 7 or i == 8 or i == 9 or i == 2 or i == 3: 
            AngleFMPA = calculate_intersection_angle(points[points_text.index('Po')], points[points_text.index('Or')],
                                                    points[points_text.index('Go')], points[points_text.index('Me')])
            
            SNMPAngle = calculate_intersection_angle(points[points_text.index('N')], points[points_text.index('S')],
                                                    points[points_text.index('Go')], points[points_text.index('Me')])
            
        elif len(points)==11:
            AB_Plane_Angle = calculate_intersection_angle(points[points_text.index('B Point')], points[points_text.index('A Point')],
                                                        points[points_text.index('Pog')], points[points_text.index('N')])
            FHPogNAngle = calculate_intersection_angle(points[points_text.index('Po')], points[points_text.index('Or')],
                                                        points[points_text.index('Pog')], points[points_text.index('N')])
            
        elif len(points)==12 and i == 3 or i == 11 or i == 6 or i == 7 :
            YFHAngle = calculate_intersection_angle(points[points_text.index('S')], points[points_text.index('Gn')],
                                                        points[points_text.index('Po')], points[points_text.index('Or')])
            
        elif len(points)==14 :
            PNS_ANS_Go_Me_Angle = calculate_intersection_angle(points[points_text.index('PNS')], points[points_text.index('ANS')],
                                                            points[points_text.index('Go')], points[points_text.index('Me')])

        if len(points) == 17:
            S_N_U1I_U1A_Angle = calculate_intersection_angle(points[points_text.index('S')], points[points_text.index('N')],
                                                            points[points_text.index('U1I')], points[points_text.index('U1A')])

        if len(points) == 19: 
            U1I_U1A_L1I_L1A_Angle = calculate_intersection_angle(points[points_text.index('U1I')], points[points_text.index('U1A')],
                                                            points[points_text.index('L1I')], points[points_text.index('L1A')])
            Go_Me_L1I_L1A_Angle = calculate_intersection_angle(points[points_text.index('Go')], points[points_text.index('Me')],
                                                            points[points_text.index('L1I')], points[points_text.index('L1A')])
    if len(points)==10 :
        print("FMPA Angle:", format_angle_value(AngleFMPA)+"°")
        print("SN-MP Angle:", format_angle_value(SNMPAngle)+"°")
    elif len(points)==11 :
        print("A-B Plane Angle:", format_angle_value(AB_Plane_Angle)+"°")
        print("Facial Angle:", format_angle_value(FHPogNAngle)+"°")
    elif len(points)==12 :
        print("Y-FH Angle:", format_angle_value(YFHAngle)+"°")
    elif len(points)==14 :
        print("MM Angle:", format_angle_value(PNS_ANS_Go_Me_Angle)+"°")
    elif len(points)==17 :
        print("U1-SN Angle:", format_angle_value(S_N_U1I_U1A_Angle)+"°")
    elif len(points)==19 :
        print("InterincisalAngle:", format_angle_value(U1I_U1A_L1I_L1A_Angle)+"°")
        print("IMPA Angle:", format_angle_value(Go_Me_L1I_L1A_Angle)+"°")
    
    # save_results()

    
def save_results():
    update_image()  # Update the image with points and lines
    with open('images/cephalometry_results.txt', 'w') as f:
        f.write("========== Cephalometry Results ==========\n")
        # Write angles_result
        for angle in angles_result.values():
            f.write(angle + "\n")
        # Write lengths_result
        for length in lengths_result.values():
            f.write(length + "\n")
        # Write between_result
        for between in between_result.values():
            f.write(between + "\n")

def undo_point():
    global points, current_point_idx
    if current_point_idx > 0:
        points.pop()
        current_point_idx -= 1
        update_image()
        if current_point_idx < len(points_text):
            prompt_next_point()

def reset_points():
    global points, current_point_idx
    points = []
    current_point_idx = 0
    update_image()

def exit_app():
    root.quit()
    root.destroy()

root = Tk()
img_label = Label(root)
img_label.pack()

Button(root, text="Open Image", command=open_image).pack()
Button(root, text="Undo Point", command=undo_point).pack()
Button(root, text="Reset Points", command=reset_points).pack()
Button(root, text="Exit", command=exit_app).pack()
img_label.bind("<Motion>", zoom)

img_label.bind("<Button-1>", click)

root.mainloop()
