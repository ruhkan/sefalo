from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.

def index(request):
    context = {
        'nama' : 'sefalometri',
    }
    return render(request, 'dashboard.html',context)


def result(request):
    # template = loader.get_template('hint.html')
    # return HttpResponse(template.render())
    return render(request, 'result.html')

def hint(request):
    return render(request, 'hint.html')