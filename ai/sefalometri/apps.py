from django.apps import AppConfig


class SefalometriConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sefalometri'
