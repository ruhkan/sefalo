Model Information:
* title:	Supportive Structures of the TMJ
* source:	https://sketchfab.com/3d-models/supportive-structures-of-the-tmj-64f8463dbc8843a6846f6499c857fb70
* author:	Mary Orczykowski (https://sketchfab.com/anatomary)

Model License:
* license type:	CC-BY-NC-4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
* requirements:	Author must be credited. No commercial use.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Supportive Structures of the TMJ" (https://sketchfab.com/3d-models/supportive-structures-of-the-tmj-64f8463dbc8843a6846f6499c857fb70) by Mary Orczykowski (https://sketchfab.com/anatomary) licensed under CC-BY-NC-4.0 (http://creativecommons.org/licenses/by-nc/4.0/)